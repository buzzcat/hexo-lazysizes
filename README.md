# hexo-lazysizes

High performance and SEO friendly lazy loader using [lazysizes](https://github.com/aFarkas/lazysizes) for [Hexo](https://hexo.io).

## Installation
``` bash
$ npm i hexo-lazysizes --save
```
## Usage
In your hexo _config.yml file.
``` yaml
lazysizes:
  enable: true
```
The plugin has several options.

## Options
``` yaml
lazysizes:
  enable: true
  loadingImage: /images/lazyloader.svg
  include_hostnames: ['buzzcat.com']
  exclude_images: ['logo.png']
  styleHelper: true
  respimg: true
  rias: true
  rias_provider: 'https://res.cloudinary.com/jettags/image/fetch/w_{width},f_auto,q_auto/'
  rias_base: 'https://buzzcat.com'
```
- **enable** - Enable hexo-lazysizes. Defaults to `false`.
- **loadingImage** - Add your own loading image.
- **include_hostnames** - Only images with these hostnames will be lazy loaded. (by default we will include all img src that begin with /).
- **exclude_images** - Some images dont play nice with lazy loading.
- **styleHelper** - Adds the simple css rule img[data-sizes="auto"] { display: block; width: 100%; }.
- **respimg** - Adds the [respimg plugin](https://github.com/aFarkas/lazysizes/tree/gh-pages/plugins/respimg).
- **rias** - Adds the [rias plugin](https://github.com/aFarkas/lazysizes/tree/gh-pages/plugins/rias).
- **rias_provider** - Your remote image provider url (must include the {width} variable for auto sizes).
- **rias_base** - The base domain against which your rias provider will pull images.

The src will be generated from the rias_provider + rias_base + original img src. For example, using the above rias settings would create the following img src for /assets/img/exampleimage.png:

```
https://res.cloudinary.com/jettags/image/fetch/w_{width},f_auto,q_auto/https://buzzcat.com/assets/img/exampleimage.png
```

The work is done during hexo generate so don't forget:
```
hexo generate
```


TODO: iframe support, gallery support