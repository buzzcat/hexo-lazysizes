'use strict';
/* global hexo */
const fs = require('hexo-fs');
const cheerio = require('cheerio');
const UglifyJS = require('uglify-js');
const lazysizesPath = require.resolve('lazysizes');
const path = require('path');
const Promise = require('bluebird');

const LazySizes = () => {
    const route = hexo.route;
    const routeList = route.list();
    const routes = routeList.filter(hpath => hpath.endsWith('.html'));
    const options = hexo.config.lazysizes;
    let mustStarts = options.include_hostnames;
    mustStarts.push('/');

    const htmls = {};
    return Promise.all(routes.map(hpath => {
        return new Promise((resolve, reject) => {
            const contents = route.get(hpath);
            let htmlTxt = '';
            contents.on('data', (chunk) => (htmlTxt += chunk));
            contents.on('end', () => {
                const $ = cheerio.load(htmlTxt, {decodeEntities: false});
                let loadingImage = '';
                if (!options.loadingImage) {
                    loadingImage = JSON.parse(fs.readFileSync(`${__dirname}/lib/loading-image.json`)).default;
                } else {
                    loadingImage = options.loadingImage;
                }

                let excludes = ':not(.lazyload):not(.lazy-ignore)';
                options.exclude_images.forEach(exclude => {
                    excludes += `:not([src*="${exclude}"])`;
                });

                $(`img${excludes}`).each((i, elem) => {
                    const match = mustStarts.findIndex((start) => {
                        return $(elem).attr('src').startsWith(start);
                    }, $(elem).attr('src'));

                    if (!($(elem).parent().get(0).tagName === 'picture') && match >= 0) {
                        const clone = $(elem).clone();
                        let rsrc = $(clone).attr('src');
                        if (options.rias) {
                            $(clone).attr('data-sizes', 'auto');
                            rsrc = `${options.rias_provider}${options.rias_base}${$(clone).attr('src')}`;
                        } else if ($(clone).is('[srcset]')) {
                            $(clone).attr('data-srcset', $(clone).attr('srcset'));
                            $(clone).attr('data-sizes', 'auto');
                            $(clone).removeAttr('srcset');
                        }
                        $(clone).attr('data-src', rsrc);
                        $(clone).attr('src', loadingImage);
                        $(clone).addClass('lazyload');
                        $(elem).after($(clone));
                        const noscript = $('<noscript></noscript>');
                        $(elem).wrap(noscript);
                    }
                });

                let injection = '';

                if (options.rias) {
                    const riasPath = path.dirname(require.resolve('lazysizes')) + '/plugins/rias/ls.rias.js';
                    let rias = fs.readFileSync(riasPath, {escape: true});
                    injection += `<script>${UglifyJS.minify(rias).code}</script>`;
                }

                if (options.respimg) {
                    const respimgPath = path.dirname(require.resolve('lazysizes')) + '/plugins/respimg/ls.respimg.js';
                    let respimg = fs.readFileSync(respimgPath, {escape: true});
                    injection += `<script>${UglifyJS.minify(respimg).code}</script>`;
                }

                let lazysizes = fs.readFileSync(lazysizesPath, {escape: true});
                injection += `<script>${UglifyJS.minify(lazysizes).code}lazySizes.init();</script>`;

                $('body').after(injection);

                let styles = fs.readFileSync(`${__dirname}/lib/nojs.css`, {escape: false});
                if (options.styleHelper) {
                    styles += fs.readFileSync(`${__dirname}/lib/style-helper.css`, {escape: false});
                }

                $('head').append(`<style>${styles}</style>`);

                htmls[hpath] = {hpath, $};
                resolve();
            });
        });
    }))
        .then(() => {
            const htmlPaths = Object.keys(htmls);
            for (const hpath of htmlPaths) {
                const html = htmls[hpath];
                route.set(hpath, html.$.html());
            }
        });
};


if (!hexo.config.lazysizes || !hexo.config.lazysizes.enable) {
    return;
}

hexo.config.lazysizes = Object.assign({
    include_hostnames: [],
    exclude_images: [],
    styleHelper: true,
    respimg: true,
    rias: false,
    rias_provider: '',
    rias_base: ''
}, hexo.config.lazysizes);

hexo.extend.filter.register('after_generate', LazySizes);
